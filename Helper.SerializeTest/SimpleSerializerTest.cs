﻿using Helper.Serialize;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using Helper.Serialize;

namespace Helper.SerializeTest
{
    /// <summary>
    ///This is a test class for SimpleClassSerializerTest and is intended
    ///to contain all SimpleClassSerializerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SimpleSerializerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for LoadFromXML
        ///</summary>
        [TestMethod()]
        public void XMLTestNotStaticClass()
        {
            TestClass test=new TestClass();
            string begStr = test.ToString();
            SimpleSerializeXML.SaveToXML(typeof(TestClass), test);
            test.SetRandomPrivate();
            string endStr = test.ToString();
            Assert.AreNotEqual(begStr, endStr);
            SimpleSerializeXML.LoadFromXML(typeof(TestClass), test);
            endStr = test.ToString();
            Assert.AreEqual(begStr,endStr);
        }

    }
}
