﻿using Helper.Serialize;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Helper.Serialize;

namespace Helper.SerializeTest
{
    
    
    /// <summary>
    ///This is a test class for SimpleSerializeSoapTest and is intended
    ///to contain all SimpleSerializeSoapTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SimpleSerializeSoapTest
    {
        /// <summary>
        ///A test for Load
        ///</summary>
        [TestMethod()]
        public void SoapSaveLoadTest()
        {
            TestClass test = new TestClass();
            string begStr = test.ToString();
            SimpleSerializeSoap soapXML=new SimpleSerializeSoap();
            soapXML.Save(test);
            test.SetRandomPrivate();
            string endStr = test.ToString();
            Assert.AreNotEqual(begStr, endStr);
            test = soapXML.Load<TestClass>();
            endStr = test.ToString();
            Assert.AreEqual(begStr, endStr);
        }
    }
}
