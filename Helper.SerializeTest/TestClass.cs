﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper.SerializeTest
{
    public enum TestEnum
    {
        None, Set1, Set2, Set3
    }

    public class TestClass
    {
        public int Set;
        public string StrSet;
        public static string StrSetStatic;

        private string _privSet;
        private static string _privSetStatic;

        public string PropertySet { get; set; }
        public string PropertyPrivSet { get; private set; }
        private string PropertyPrivAllSet { get; set; }

        private bool _privSetBool;
        private double _privSetDouble;
        private byte _privSetByte;
        private ulong _privSetULong;
        private decimal _privSetDecimal;
        private float _privSetFloat;

        private TestEnum _priveEnum;
        private int[] intArray;
        // +struct
        // +class
        // +массивы

        public TestClass()
        {
            Set = 9999;
            StrSet = "public_test";
            _privSet = "private_test";
            _privSetStatic = "privateStatic_test";
            StrSetStatic = "publicStatic_test";
            PropertySet = "propetry_test";
            PropertyPrivSet = "propertyPrivSet_test";
            PropertyPrivAllSet = "propertyPrivAllSet_test";
            _privSetBool = true;
            _privSetDouble = 23231.345;
            _privSetByte = 200;
            _privSetULong = 222222222222444421;
            _privSetDecimal = 232323;
            _privSetFloat = (float)123.23;
            _priveEnum = TestEnum.Set3;
            intArray = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        }

        public void SetRandomPrivate()
        {
            Random rand = new Random();
            Set = rand.Next(100, 1000);
            StrSet = rand.Next(100, 1000).ToString();
            StrSetStatic = rand.Next(100, 1000).ToString();
            _privSet = rand.Next(100, 1000).ToString();
            _privSetStatic = rand.Next(100, 1000).ToString();
            PropertySet = rand.Next(100, 1000).ToString();
            PropertyPrivSet = rand.Next(100, 1000).ToString();
            PropertyPrivAllSet = rand.Next(100, 1000).ToString();
            _privSetBool = false;
            _privSetDouble = (double)rand.Next(100, 1000) / 10;
            _privSetByte = (byte)rand.Next(1, 200);
            _privSetULong = (ulong)rand.Next(100, 1000) * 10000;
            _privSetDecimal = rand.Next(100, 1000) * 1000000;
            _privSetFloat = (float)rand.Next(100, 1000) / 10;
            _priveEnum = TestEnum.Set2;
            intArray = new int[10] { 10, 22, 33, 44, 35, 56, 67, 78, 89, 1110 };
        }

        public override string ToString()
        {
            return string.Format("Set={0}, StrSet={1}, StrSetStatic={2}, PrivSet={3}, PrivSetStatic={4}, " +
                "PropertySet={5}, PropertyPrivSet={6}, PropertyPrivAllSet={7}" +
                "_privSetBool={8}, _privSetDouble={9}, _privSetByte={10}, _privSetULong={11}, _privSetDecimal={12}, _privSetFloat={13}, " +
                "_priveEnum = {14}, intArray(Sum)={15}",
                Set, StrSet, StrSetStatic, _privSet, _privSetStatic, PropertySet, PropertyPrivSet, PropertyPrivAllSet,
                _privSetBool, _privSetDouble, _privSetByte, _privSetULong, _privSetDecimal, _privSetFloat, _priveEnum.ToString(), intArray.Sum().ToString());
        }
    }
}
