﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper.Serialize
{
    public interface ISimpleSerialize
    {
        void Save<T>(T classToSave, string fileName = null) where T : class;
        T TryLoad<T>(string fileName = null) where T : class;
        T Load<T>(string fileName = null) where T : class, new();
        // void Load<T>(T classToLoad, string fileName = null) where T : class, new();
    }
}
