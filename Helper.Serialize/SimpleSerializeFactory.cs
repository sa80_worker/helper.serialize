﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Helper.FileUtils;

namespace Helper.Serialize
{
    public enum SerializeType
    {
        XML, Soap,

        /// <summary>
        /// Binary serialization format.
        /// </summary>
        Binary,
    }


    
    /// <summary>
    /// Класс для наследования, если в классе нет других претендентов на наследование
    /// Внимание! Класс использует только конструктор по умолчанию
    /// </summary>
    [Serializable]
    public abstract class SimpleSerializeFactory
    {
        private readonly ISimpleSerialize _serialize;
        protected SimpleSerializeFactory(SerializeType type)
        {
            _serialize = GetSerialize(type);
        }


        private static ISimpleSerialize GetSerialize(SerializeType type)
        {
            switch (type)
            {
                case SerializeType.XML:
                    return new SimpleSerializeXML();
                case SerializeType.Soap:
                    return new SimpleSerializeSoap();
                default:
                    throw new Exception("Не реализован тип сериализации");
            }
        }

        //public void Save<T>(string fileName = null) where T : class, new()
        //{
        //    if (_serialize != null)
        //        _serialize.Save<T>(this,fileName);
        //    else
        //        throw new SerializeNotInitException(this);
        //}

        public static T Load<T>(SerializeType type, string fileName = null) where T : class, new()
        {
            var serialize = GetSerialize(type);
            if (serialize != null)
            {
                T loadClass = serialize.Load<T>(fileName);
                return loadClass ?? (new T());
            }
            else
                throw new SerializeNotInitException();
        }

    }
}
