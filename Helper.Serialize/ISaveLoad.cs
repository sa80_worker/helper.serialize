﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper.Serialize
{
    /// <summary>
    /// Интерфейс для записи/счтитывания класса в файл (создан для быстрого генерирования имен классов :) )
    /// </summary>
    public interface ISaveLoad
    {
        void Save(string fileName = null);
        void Load(string fileName = null);
    }

}
