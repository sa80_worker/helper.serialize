﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Helper.FileUtils;

namespace Helper.Serialize
{
    public abstract class SimpleSerialize : ISimpleSerialize
    {
        /// <summary>
        /// Имя файла - по умолчанию имя класса 
        /// </summary>
        protected static string GetFileName(string fileExtension, Type classType, string fileName=null)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return string.Format("{0}{1}.{2}", FileUtil.CurrentDirectory(), classType.Name, fileExtension);

            string path = Path.GetDirectoryName(fileName);
            return string.IsNullOrEmpty(path)
                       ? string.Format("{0}{1}.{2}", FileUtil.CurrentDirectory(), classType.Name, fileExtension)
                       : string.Format("{0}.{1}", fileName, fileExtension);
        }

        public abstract void Save<T>(T classToSave, string fileName = null) where T : class;
        public abstract T TryLoad<T>(string fileName = null) where T : class;
        public abstract T Load<T>(string fileName = null) where T : class, new();
        // public abstract void Load<T>(T classToLoad, string fileName = null) where T : class, new();
    }
}
