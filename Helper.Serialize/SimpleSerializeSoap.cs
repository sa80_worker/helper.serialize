﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using Helper.FileUtils;

namespace Helper.Serialize
{
    [Serializable]
    public sealed class SimpleSerializeSoap : SimpleSerialize
    {
        public const string FileExtension="soap";

        private static void SaveToSoap(object className, string fileName) 
        {
            if (className == null)
                throw new Exception("Записываемый класс не может быть равен null");
            try
            {
                string fileSave = GetFileName(FileExtension, className.GetType(), fileName);
                using (var fileStream = new FileStream(fileSave, FileMode.Create, FileAccess.Write))
                {
                    SoapFormatter soap = new SoapFormatter();
                    soap.Serialize(fileStream,className);
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Ошибка при сохранении класса, " + className.GetType().Name, exception);
            }
        }

        /// <summary>C
        /// Сохранение класса в формате Soap
        /// </summary>
        /// <typeparam name="T">Тип класса</typeparam>
        /// <param name="classToSave">указатель</param>
        /// <param name="fileName">Имя файла, если пустое, то берется такое же как и у имени класса</param>
        public override void Save<T>(T classToSave, string fileName = null)
        {
            SaveToSoap(classToSave, fileName);
        }

        /// <summary>
        /// Чтение класса из формата Soap (на выходе всегда класс)
        /// </summary>
        /// <typeparam name="T">Тип класса</typeparam>
        /// <param name="fileName">Имя файла, если пустое, то берется такое же как и у имени класса</param>
        public override T Load<T>(string fileName=null)
        {
            return LoadFromSoap<T>(fileName) ?? new T();
        }

        /// <summary>
        /// Чтение класса из формата Soap (null если не удалось прочитать)
        /// </summary>
        /// <typeparam name="T">Тип класса</typeparam>
        /// <param name="fileName">Имя файла, если пустое, то берется такое же как и у имени класса</param>
        public override T TryLoad<T>(string fileName = null)
        {
            return LoadFromSoap<T>(fileName);
        }


        private static T LoadFromSoap<T>(string fileName) where T : class
        {
            // var classType = classToLoad.GetType();
            var classType = typeof(T);
            string fileLoad = GetFileName(FileExtension, classType, fileName);
            if (!File.Exists(fileLoad))
                return null;
            using (var fileStream = new FileStream(fileLoad, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    SoapFormatter soap = new SoapFormatter();
                    var des = soap.Deserialize(fileStream) as T;
                    return des;
                }
                catch (Exception exception)
                {
                    FileUtil.RenameToBackFile(fileName); // ошибочный файл переименовываем 
                    throw;
                }
            }
        }
    }
}
