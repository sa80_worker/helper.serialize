﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper.Serialize
{
    public class SerializeNotInitException:Exception
    {
        public SerializeNotInitException()
        {
            throw new Exception("Класс для сериализации не реализован");
        }

        public SerializeNotInitException(object thisClass)
        {
            throw new Exception("Класс для сериализации не реализован. Класс " + thisClass.GetType().Name);
        }

    }
}
