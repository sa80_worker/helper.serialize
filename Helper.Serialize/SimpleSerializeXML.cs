﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
// using System.Runtime.Serialization;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
//using System.Runtime.Serialization.DataContractSerializer;

namespace Helper.Serialize
{
    /// <summary>
    /// Запись класса в формате XML
    /// </summary>
    public sealed class SimpleSerializeXML: SimpleSerialize
    {
        public const string FileExtension = "xml";

        #region Method
        public override void Save<T>(T classToSave, string fileName = null)
        {
            if (classToSave == null)
                throw new Exception("Записываемый класс не может быть равен null");
            try
            {
                string fileSave = GetFileName(FileExtension, classToSave.GetType(), fileName);
                SaveToDocumentFormat<T>(classToSave, fileSave);
            }
            catch (Exception exception)
            {
                throw new Exception("Ошибка при сохранении класса, " + classToSave.GetType().Name, exception);
            }
        }

        private static void SaveToDocumentFormat<T>(T serializableObject, string path)
        {
            var type = serializableObject.GetType();
            var serializer = GetDataContractSerializer(type);
            using (FileStream tw = new FileStream(path, FileMode.Create))
            {
                serializer.WriteObject(tw, serializableObject);
            }
        }

        /// <summary>
        /// Чтение класса из формата XML (на выходе всегда класс)
        /// </summary>
        /// <typeparam name="T">Тип класса</typeparam>
        /// <param name="fileName">Имя файла, если пустое, то берется такое же как и у имени класса</param>
        public override T Load<T>(string fileName = null)
        {
            T classToLoad = TryLoad<T>(fileName);
            return classToLoad ?? new T();
        }

        /// <summary>
        /// Чтение класса из формата XML (null если не удалось прочитать)
        /// </summary>
        /// <typeparam name="T">Тип класса</typeparam>
        /// <param name="fileName">Имя файла, если пустое, то берется такое же как и у имени класса</param>
        public override T TryLoad<T>(string fileName = null)
        {
            T classToLoad;
            string fileLoad = GetFileName(FileExtension, typeof(T), fileName);
            if (!File.Exists(fileLoad))
                return null;

            using (FileStream fs = new FileStream(fileLoad, FileMode.Open))
            {
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                var serializer = GetDataContractSerializer(typeof(T));
                // Deserialize the data and read it from the instance.
                classToLoad = serializer.ReadObject(reader, true) as T;
                reader.Close();
            }
            return classToLoad;
        }

        private static DataContractSerializer GetDataContractSerializer(Type type)
        {
            DataContractSerializer serializer = new DataContractSerializer(type, type.Name,"http://schemas.datacontract.org/2004/07/");
            return serializer;
        }
        #endregion 
    }
}
