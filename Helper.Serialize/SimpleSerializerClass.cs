﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Helper.Extensions.Strings;

namespace Helper.Serialize
{

    /// <summary>
    /// Класс для сохранения значения свойств класса в словарь 
    /// </summary>
    public class SimpleSerializerClass
    {
        /// <summary>
        /// Глубина выборки атрибутов
        /// </summary>
        public readonly int DeepFetch;

        /// <summary>
        /// Класс в который заполняются все свойства
        /// </summary>
        public readonly StringDictionary DictSerialize;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="deepFetch">Глубина выборки атрибутов</param>
        public SimpleSerializerClass(int deepFetch):this()
        {
            DeepFetch = deepFetch;
        }

        public SimpleSerializerClass()
        {
            DeepFetch = 1;
            DictSerialize = new StringDictionary();
        }

        /// <summary>Записывает все публичные свойства класса в словарь</summary>
        /// <param name="baseClass">Класс, публичные свойства которых записываются в словарь</param>
        /// <param name="prefix"></param>
        /// <returns>Словарь. где ключ - название property, значение - значение property</returns>
        public StringDictionary GetProperty(object baseClass, string prefix)
        {
            StringDictionary dict = new StringDictionary();
            GetProperty(baseClass, 0, prefix, dict);
            foreach (DictionaryEntry valuePair in dict)
            {
                AddUpdateDictionary(DictSerialize, valuePair.Key.ToString(), valuePair.Value.ToString());
            }
            return dict;
        }

        /// <summary>
        /// Возвращает отформатированную строку типа IsEveningTrade=true;
        /// </summary>
        /// <param name="comma">Разделитель</param>
        public string ToString(char comma)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DictionaryEntry value in DictSerialize)
            {
                sb.Append(value.Key);
                sb.Append("=");
                sb.Append(value.Value);
                sb.Append(comma);
            }
            return sb.ToString();
        }

        /// <summary>Записывает все публичные свойства класса в словарь</summary>
        /// <param name="baseClass">Класс, публичные свойства которых записываются в словарь</param>
        /// <param name="currentDeepFetch">Глубина выборки значений классов</param>
        /// <param name="prefix"></param>
        /// <param name="dict"></param>
        /// <returns>Словарь. где ключ - название property, значение - значение property</returns>
        private void GetProperty(object baseClass, int currentDeepFetch, string prefix, StringDictionary dict)
        {
            if (baseClass == null)
                return;
            if (currentDeepFetch > 1) // глубина рекурсии 1
                return;
            Type classType = baseClass.GetType();
            const BindingFlags getBindingFlag =
                BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public;
            foreach (PropertyInfo p in classType.GetProperties(getBindingFlag))
            {
                if (!p.CanRead)
                    continue;
                var valObject = p.GetValue(baseClass, null);
                if (valObject == null)
                    continue;
                if (Attribute.IsDefined(p, typeof(NonSerializedAttribute)))
                    continue;
                if (p.PropertyType.IsValueType || (valObject is String))
                {
                    try
                    {
                        string strVal = Convert.ToString(valObject);
                        if (string.IsNullOrEmpty(prefix))
                            AddUpdateDictionary(dict, p.Name, strVal);
                        else
                            AddUpdateDictionary(dict, prefix + "." + p.Name, strVal);
                    }
                    catch
                    { }
                    continue;
                }
                if (p.PropertyType.IsClass)
                {
                    GetProperty(valObject, currentDeepFetch + 1, valObject.GetType().Name, dict);
                }
            }
        }

        // Преобразует строку к ключам 
        public void FromStringToDictionary(string parserString, char comma)
        {
            // @"IsEveningTrade=(\w*);IsDayTrade=False;IsMonitoring=False;RateChangeAll=0;StatusMonitoring=Skipped;Deposit=5656;Code=FZ0007S;Account=;FIO=Саврасов Андрей Владимирович;Exchange=RTS;CurrentMoney=500000;MorningMoney=500000;RateChange=0;AccumulateMoney=0;PrepareTo=CLIENT_CODE=FZ0007S;"
            //var res = Regex.Matches(parserString, @"((\w|\W)*)=(\w*)" + comma);
            //foreach (Match match in res)
            //{
            //    string[] strVal = match.Value.TrimEnd(comma).Split('=');
            //    if (strVal.Length==2)
            //        AddUpdateDictionary(DictSerialize, strVal[0], strVal[1]);
            //}
            var res = parserString.Split(comma);
            foreach (string str in res)
            {
                if (str.IsEmpty())
                    continue;
                string[] strVal = str.TrimEnd(comma).Split('=');
                if (strVal.Length==2)
                    AddUpdateDictionary(DictSerialize, strVal[0], strVal[1]);
            }
        }

        private void AddUpdateDictionary(StringDictionary dict, string key, string value)
        {
            if (dict.ContainsKey(key))
                dict[key] = value;
            else
                dict.Add(key, value);
        }
    }
}
